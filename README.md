## 深度学习实战

###  深度学习实战电路板缺陷检测 
- https://mbd.pub/o/bread/mbd-ZpuWkphr
###  深度学习实战TT100K中国交通标志检测 
- https://mbd.pub/o/bread/mbd-Zpuam59y
###  深度学习实战人脸表情识别
- https://mbd.pub/o/bread/mbd-ZJ2XmZ9q
###  深度学习实战肺部医疗图像分割 
- https://mbd.pub/o/bread/mbd-ZpaTm55t
###  深度学习实战CCTSDB中国交通标志检测 
- https://mbd.pub/o/bread/mbd-ZJybl5ht
###  深度学习实战手写数字识别 
- https://mbd.pub/o/bread/mbd-ZpWXkptv
###  基于深度学习的文字识别 
- https://mbd.pub/o/bread/mbd-ZJybk5Zt
###  基于深度学习的多焦点图像融合系统 
- https://mbd.pub/o/bread/ZpyXkptx
###  深度学习实战人脸检测与识别 
- https://mbd.pub/o/bread/mbd-Z5iak51s
###  深度学习实战图像缺陷修复 
- https://mbd.pub/o/bread/Z5iamZ9r
### 深度学习实战老照片上色
- https://mbd.pub/o/bread/Z5uVmJxy
### 树莓派部署深度学习车牌识别
- https://mbd.pub/o/bread/Z5yVmp5v
### 深度学习实战车辆目标跟踪
- https://mbd.pub/o/bread/Z5ybk55s
### 深度学习实战行人检测与跟踪
- https://mbd.pub/o/bread/Z5ycmp1y
### 深度学习实战车辆目标追踪与撞线计数
- https://mbd.pub/o/bread/Z5yblZhu

### MATLAB深度学习实战PCB缺陷检测
- https://mbd.pub/o/bread/Z5ybmZtw

### MATLAB深度学习实战文字识别
- https://mbd.pub/o/bread/Z52bk5pv

### MATLAB深度学习实战车牌识别
- https://mbd.pub/o/bread/Z52bmZ5q
### 深度学习实战文字识别-网页版
- https://mbd.pub/o/bread/Z52ck5dv
### 深度学习实战电影推荐系统
- https://mbd.pub/o/bread/Z52cmpxy

### 树莓派深度学习实战文字识别-网页版
- https://mbd.pub/o/bread/Z56Tkp5x
### 深度学习实战车道线检测
- https://mbd.pub/o/bread/aJWbmJ5s